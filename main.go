package main

import (
  "net/http"
  "fmt"
)

func main() {
  http.Handle("/user/auth", api.Auth)
  http.ListenAndServe(":1111", nil)
}
